package com.drone.drone.repositories;

import com.drone.drone.entities.MedicalDelivery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneDeliveryRepository extends JpaRepository<MedicalDelivery, String> {

}
