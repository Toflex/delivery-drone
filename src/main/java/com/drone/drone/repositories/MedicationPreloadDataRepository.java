package com.drone.drone.repositories;

import com.drone.drone.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MedicationPreloadDataRepository extends JpaRepository<Medication, String> {

	
	
}
