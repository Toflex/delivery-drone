package com.drone.drone.entities;

import java.math.BigDecimal;

public interface DroneBatteryLevel {
     String getSerialNumber();
     BigDecimal getBattery();

}
