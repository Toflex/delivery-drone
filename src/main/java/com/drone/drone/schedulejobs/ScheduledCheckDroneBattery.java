package com.drone.drone.schedulejobs;

import java.text.DecimalFormat;
import java.util.List;

import com.drone.drone.entities.Drone;
import com.drone.drone.repositories.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@SpringBootApplication
@EnableScheduling
@EnableAsync
public class ScheduledCheckDroneBattery {
	
	@Autowired
	private DroneRepository droneRepository;
	
	static final Logger log = LoggerFactory.getLogger(ScheduledCheckDroneBattery.class);
	
    @Scheduled(fixedRate = 5000)
    public void scheduleFixedRateTaskAsync() throws InterruptedException {
        
        List<Drone> arrDroneBatteryLevels = droneRepository.findAll();
        
    	DecimalFormat decFormat = new DecimalFormat("#%");
        for(int i=0; i<arrDroneBatteryLevels.size(); i++) {
            
        	log.info("Battery level--: SerialNumber  "+ arrDroneBatteryLevels.get(i).getSerialNumber()+"  Battery Level "+ decFormat.format(arrDroneBatteryLevels.get(i).getBattery()));
        }
        Thread.sleep(5000);

    }
    
}
