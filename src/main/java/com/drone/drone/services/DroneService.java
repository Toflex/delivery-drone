package com.drone.drone.services;

import com.drone.drone.dto.request.DroneDeliveryRequest;
import com.drone.drone.dto.request.DroneGetBatteryRequest;
import com.drone.drone.dto.request.DroneRegisterRequest;
import com.drone.drone.dto.request.LoadDroneRequest;
import com.drone.drone.dto.response.*;
import org.springframework.stereotype.Component;

@Component
public interface DroneService {
	
	RegisterDroneResponse register(DroneRegisterRequest drone);

	DroneBatteryDetailsResponse getBateryLevel(DroneGetBatteryRequest drequest) throws Exception;
	
	DroneMedicationLoadResponse getLoadedMedicationForADrone(String serialno);
	
	AvailableDroneResponse getAvailabeDrones();
	
	LoadDroneResponse loadDrone(LoadDroneRequest loadRequest);
	
	DeliverDroneResponse deliverLoad(DroneDeliveryRequest loadRequest);
	
	void preLoadData();

	
}
